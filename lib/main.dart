import 'package:flutter/material.dart';

void main() => runApp(
      MaterialApp(home: Home()),
    );

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Center(
          child: Text(
            'Kupa Project',
            style: TextStyle(
              color: Colors.red[600],
            ),
          ),
        ),
      ),
      body: Container(
        color: Colors.black,
        padding: EdgeInsets.symmetric(horizontal: 127.35, vertical: 311),
        child: RaisedButton(
          onPressed: () {
            print("clicked");
          },
          color: Colors.red[600],
          child: Text(
            "click",
            style: TextStyle(
              fontSize: 50.0,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}
